"""
WSGI config for conf project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

import cherrypy

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "kubedjango.settings")

application = get_wsgi_application()

# Mount the application
cherrypy.tree.graft(application, "/")

# Unsubscribe the default server
cherrypy.server.unsubscribe()

# Instantiate a new server object
server = cherrypy._cpserver.Server()

# Configure the server object
server.socket_host = "0.0.0.0"
server.socket_port = 8080
server.thread_pool = 30

# Subscribe this server
server.subscribe()

cherrypy.engine.start()
cherrypy.engine.block()
